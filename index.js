const { CommandoClient } = require('discord.js-commando');
const path = require('path');

const client = new CommandoClient({
    commandPrefix: '?'
});

client.registry
    .registerDefaultTypes()
    .registerGroups([
        ['first', 'Your First Command Group'],
    ])
    .registerCommandsIn(path.join(__dirname, 'commands'));

client.once('ready', () => {
    console.log(`Logged in as ${client.user.tag}! (${client.user.id})`);
});

client.on('error', console.error);

client.login('NzA5MTA2MTgxOTU1NjQ5NjI1.XrhE5Q.-1DZyD-Ll6q0d5yrSxZxAzxwCF4');
