const { CommandoClient, Command } = require('discord.js-commando');

class InfoCommand extends Command {
    constructor(bot) {
        super(bot, {
            name: 'info',
            aliases: ['i'],
            group: 'first',
            memberName: 'info',
            description: 'shows you information about me.'
        });
    }

    async run(msg, args) {
        return msg.channel.send("We're live!")
    }
}


module.exports = InfoCommand
